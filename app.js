//generic Import
const express = require("express");
const mongoose = require("mongoose");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const expressValidator = require("express-validator");
require ("dotenv").config();


// import routes
const authRoutes = require("./routes/auth");
const fileRoutes = require("./routes/file");
const relationRoutes = require("./routes/relation");
const tweetRoutes = require("./routes/tweet");
const userRoutes = require("./routes/user");

// app - express
const app = express();

// db
/*mongoose 
    .connect(process.env.DATABASE,{
        useNewUrlParser: true,
        useCreateIndex: true
        })
        .then (()=> console.log("DB Connected"));
*/

//modern Connection

const db = async () => {
    try {
        const success = await mongoose.connect(process.env.DATABASE, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false
        });
        console.log('DB Connected');

    }
    catch (error){
        console.log('DB Connection Error', error);
    }
}

db();

// Middleware
app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(expressValidator());
app.use(cors());

//routes Middlewares

app.use("/api", authRoutes);
app.use("/api", fileRoutes);
app.use("/api", relationRoutes);
app.use("/api", tweetRoutes);
app.use("/api", userRoutes);



//port
const port = process.env.PORT || 8000;

//listen Port
app.listen(port, ()=> {
    console.log(`Server is running on port ${port}`);
    });