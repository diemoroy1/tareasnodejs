const express = require("express");
const router = express.Router(); 

const { tweetById } = require("../controllers/tweet.js");
const { tweetsMessage } = require("../validator");


router.get("/tweets");
router.get("/tweets/followers");
router.post("/tweets/create",tweetsMessage);
router.delete("/tweets/:tweetId");


// params
router.param("tweetId", tweetById);

module.exports = router;