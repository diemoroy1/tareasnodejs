const express = require("express");
const router = express.Router();

const { userSignupValidator, userLoginValidator } = require("../validator");


router.post("/sign-up", userSignupValidator);
router.post("/log-in", userLoginValidator);

module.exports = router;