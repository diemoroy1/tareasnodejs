const mongoose = require("mongoose");
const {ObjectId } = mongoose.Schema;

const RelationsSchema = new mongoose.Schema(
    {
        userId: {
            type: ObjectId,
            ref: "Users",
            required: true
        },
        userRelationId: {
            type: ObjectId,
            ref: "Users",
            required: true
        }
        
    },
    {timestamps:true}
);

 module.exports  = mongoose.model("Relations", RelationsSchema);

